import './App.css';
import Carousel from './components/Carousel/Carousel';
import Footer from './components/Footer/Footer';
import Header from './components/Header/Header';
import SideScrole from './components/SideScrole/SideScrole';

function App() {
  return (
    <>
      <div className='sticky-top'>
        <Header />
      </div>
      <Carousel />
      <div className='fixbg'></div>
      <SideScrole />
      <div style={{backgroundColor: '#232f3e'}}>
        <Footer />
      </div>
    </>
  );
}

export default App;
