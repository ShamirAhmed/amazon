import React, { Component } from 'react'

class Card extends Component {

    render() {
        return (
            <>
                {/* <div className='p-3 w-25' style={{border: '1px solid black'}}>
                    <h4 className='w-75'>{this.props.titel}</h4>
                    <div className='row d-flex align-items-center'>
                        <div className='column d-flex flex-column w-25'>
                            <img src={this.props.img[0]} alt='book' />
                            <label style={{fontSize: 12}}>{this.props.labelName[0]}</label>
                        </div>
                        <div className='column d-flex flex-column w-25'>
                            <img src={this.props.img[1]} alt='book' />
                            <label style={{fontSize: 12}}>{this.props.labelName[1]}</label>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='column d-flex flex-column w-25'>
                            <img src={this.props.img[2]} alt='book' />
                            <label style={{fontSize: 12}}>{this.props.labelName[2]}</label>
                        </div>
                        <div className='column d-flex flex-column w-25'>
                            <img src={this.props.img[3]} alt='book' />
                            <label style={{fontSize: 12}}>{this.props.labelName[3]}</label>
                        </div>
                    </div>
                </div> */}
                <div class="card w-auto" style={{ width: '18rem' }}>
                    <div class="card-body p-3">
                        <h5 class="card-title">{this.props.titel}</h5>
                        <div className='row p-3'>
                            <div className='col d-flex flex-column w-25'>
                                <img src={this.props.img[0]} alt='book' width='100px' height='150px'/>
                                <label style={{ fontSize: 12 }}>{this.props.labelName[0]}</label>
                            </div>
                            <div className='col d-flex flex-column w-25'>
                                <img src={this.props.img[1]} alt='book' width='100px' height='150px'/>
                                <label style={{ fontSize: 12 }}>{this.props.labelName[1]}</label>
                            </div>
                        </div>
                        <div className='row p-3'>
                            <div className='col d-flex flex-column w-25'>
                                <img src={this.props.img[2]} alt='book' width='100px' height='150px'/>
                                <label style={{ fontSize: 12 }}>{this.props.labelName[2]}</label>
                            </div>
                            <div className='col d-flex flex-column w-25'>
                                <img src={this.props.img[3]} alt='book' width='100px' height='150px'/>
                                <label style={{ fontSize: 12 }}>{this.props.labelName[3]}</label>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default Card;