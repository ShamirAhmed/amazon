import React, { Component } from 'react'
import Content from './Content';

class Carousel extends Component {
    render() {
        return (
            <>
                <div id="carouselExampleInterval" className="carousel slide position-relative" data-bs-ride="carousel">
                    <div className="carousel-inner">
                        <div className="carousel-item active" data-bs-interval="5000">
                            <img src="https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/51SbP3Z81ZL._SX3000_.jpg" className="d-block w-100" alt="..." />
                        </div>
                        <div className="carousel-item" data-bs-interval="5000">
                            <img src="https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/61MfIKliTqL._SX3000_.jpg" className="d-block w-100" alt="..." />
                        </div>
                        <div className="carousel-item" data-bs-interval="5000">
                            <img src="https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/61Bqd7f3NAL._SX3000_.jpg" className="d-block w-100" alt="..." />
                        </div>
                        <div className="carousel-item" data-bs-interval="5000">
                            <img src="https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/71OBp5QcAKL._SX3000_.jpg" className="d-block w-100" alt="..." />
                        </div>
                        <div className="carousel-item" data-bs-interval="5000">
                            <img src="https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/61k2pydRT9L._SX3000_.jpg" className="d-block w-100" alt="..." />
                        </div>
                        <div className="carousel-item" data-bs-interval="5000">
                            <img src="https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/81cVCvvnr8L._SX3000_.jpg" className="d-block w-100" alt="..." />
                        </div>
                        <div className="carousel-item" data-bs-interval="5000">
                            <img src="https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/516-iHvhtmL._SX3000_.jpg" className="d-block w-100" alt="..." />
                        </div>
                    </div>
                    <button className="carousel-control-prev d-flex align-items-center px-3 bg-dark h-25 w-auto" type="button" data-bs-target="#carouselExampleInterval" data-bs-slide="prev">
                        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span className="visually-hidden">Previous</span>
                    </button>
                    <button className="carousel-control-next d-flex align-items-center px-3 bg-dark h-25 w-auto" type="button" data-bs-target="#carouselExampleInterval" data-bs-slide="next">
                        <span className="carousel-control-next-icon" aria-hidden="true"></span>
                        <span className="visually-hidden">Next</span>
                    </button>
                    <div className='position-absolute top-50 w-100' style={{backgroundImage: 'linear-gradient(180deg, rgba(0, 0, 0, 0), #ffffff)'}}>
                        <Content />
                    </div>
                </div>
            </>
        )
    }
}

export default Carousel;