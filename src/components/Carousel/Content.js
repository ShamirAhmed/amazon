import React, { Component } from 'react'
import Card from './Card';

class Content extends Component {
    render() {
        return (
            <>
                <div className='d-flex justify-content-evenly'>
                    <Card titel='Keep shopping for'
                        img={
                            [
                                'https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/91bYsX41DVL._AC_SY135_.jpg',
                                'https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/71g2ednj0JL._AC_SY135_.jpg',
                                'https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/81l3rZK4lnL._AC_SY135_.jpg',
                                'https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/71sBtM3Yi5L._AC_SY135_.jpg'
                            ]
                        }
                        labelName={
                            [
                                "Atomic Habits: The lif…",
                                "The Psychology of M…'",
                                "Ikigai: The Japanese s…",
                                "The Power of Your Sub…"
                            ]
                        } />
                    <Card titel='Now India will say "Got it on Amazon!"' 
                    img={
                        [
                            'https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/91bYsX41DVL._AC_SY135_.jpg',
                            'https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/71g2ednj0JL._AC_SY135_.jpg',
                            'https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/81l3rZK4lnL._AC_SY135_.jpg',
                            'https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/71sBtM3Yi5L._AC_SY135_.jpg'
                        ]
                    }
                    labelName={
                        [
                            "Atomic Habits: The lif…",
                            "The Psychology of M…'",
                            "Ikigai: The Japanese s…",
                            "The Power of Your Sub…"
                        ]
                    }
                    />
                    <Card titel='Pick up where you left off' 
                    img={
                        [
                            'https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/91bYsX41DVL._AC_SY135_.jpg',
                            'https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/71g2ednj0JL._AC_SY135_.jpg',
                            'https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/81l3rZK4lnL._AC_SY135_.jpg',
                            'https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/71sBtM3Yi5L._AC_SY135_.jpg'
                        ]
                    }
                    labelName={
                        [
                            "Atomic Habits: The lif…",
                            "The Psychology of M…'",
                            "Ikigai: The Japanese s…",
                            "The Power of Your Sub…"
                        ]
                    }
                    />
                    <Card 
                    img={
                        [
                            'https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/91bYsX41DVL._AC_SY135_.jpg',
                            'https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/71g2ednj0JL._AC_SY135_.jpg',
                            'https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/81l3rZK4lnL._AC_SY135_.jpg',
                            'https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/71sBtM3Yi5L._AC_SY135_.jpg'
                        ]
                    }
                    labelName={
                        [
                            "Atomic Habits: The lif…",
                            "The Psychology of M…'",
                            "Ikigai: The Japanese s…",
                            "The Power of Your Sub…"
                        ]
                    }
                    />
                </div>
            </>
        )
    }
}

export default Content;