import React, { Component } from 'react'

class Search extends Component {
    render() {
        return (
            <>
                <div className="input-group h-25">
                    <button className="btn btn-outline-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">All</button>
                    <ul className="dropdown-menu">
                        <li><a className="dropdown-item" href="/">Action</a></li>
                        <li><a className="dropdown-item" href="/">Another action</a></li>
                        <li><a className="dropdown-item" href="/">Something else here</a></li>
                        <li><hr className="dropdown-divider" /></li>
                        <li><a className="dropdown-item" href="/">Separated link</a></li>
                    </ul>
                    <input type="text" className="form-control" aria-label="Text input with dropdown button" />
                    <button className="btn btn-outline-secondary" type="button" id="button-addon2">
                        <i className="fa-solid fa-magnifying-glass"></i>
                    </button>
                </div>
            </>
        )
    }
}

export default Search;