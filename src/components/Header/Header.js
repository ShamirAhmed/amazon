import React, { Component } from 'react'
import Search from './Search'

class Header extends Component {
    render() {
        return (
            <>
                <nav className="navbar navbar-expand-lg navbar-dark bg-dark d-flex align-items-center">
                    <div className="container-fluid">
                        <a className="navbar-brand" href="/">Amazon</a>
                        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul className="navbar-nav me-auto d-flex align-items-center w-100">
                                <li className="nav-item">
                                    <a className="nav-link active" aria-current="page" href="/">Deliver to</a>
                                </li>
                                {/* <li className="d-flex" role="search">
                                    <input className="form-control me-2" type="search" placeholder="Search" aria-label="Search" />
                                    <button className="btn btn-outline-success" type="submit">Search</button>
                                </li> */}
                                <Search />
                                <li className="nav-item dropdown w-25">
                                    <div className='container w-50'>
                                        <a className="nav-link dropdown-toggle w-50" href="/" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                            <img className='w-50' src='https://cdn.pixabay.com/photo/2012/04/10/23/03/india-26828__340.png' alt='img' />
                                        </a>
                                        <ul className="dropdown-menu">
                                            <li><a className="dropdown-item" href="/">Action</a></li>
                                            <li><a className="dropdown-item" href="/">Another action</a></li>
                                            <li><hr className="dropdown-divider" /></li>
                                            <li><a className="dropdown-item" href="/">Something else here</a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li className="nav-item dropdown">
                                    <a className="nav-link dropdown-toggle active" href="/" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                        Account & List
                                    </a>
                                    <ul className="dropdown-menu">
                                        <li><a className="dropdown-item" href="/">Action</a></li>
                                        <li><a className="dropdown-item" href="/">Another action</a></li>
                                        <li><hr className="dropdown-divider" /></li>
                                        <li><a className="dropdown-item" href="/">Something else here</a></li>
                                    </ul>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="/">Returns&Orders</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="/">
                                        <i className="fa-solid fa-cart-shopping"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </>
        )
    }
}

export default Header