import React, { Component } from 'react';
import logo from '../Images/amazonlogo.png';

class MidFooter extends Component {
    render() {
        return (
            <>
                <div className="container">
                    <footer className="pt-2">
                        <div className='d-flex justify-content-center align-items-center my-3'>
                            <div className='w-25'>
                                <img className='w-25' src={logo} alt='logo'></img>
                            </div>
                            <div className="dropdown">
                                <button className="btn btn-outline-secondary dropdown-toggle text-light" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    English
                                </button>
                                <ul className="dropdown-menu">
                                    <li><a className="dropdown-item" href="/">Action</a></li>
                                    <li><a className="dropdown-item" href="/">Another action</a></li>
                                    <li><a className="dropdown-item" href="/">Something else here</a></li>
                                </ul>
                            </div>
                        </div>
                        <ul style={{ fontSize: 12 }} className="nav justify-content-center">
                            <li className="nav-item"><a href="/" className="nav-link px-2 text-light">Australia</a></li>
                            <li className="nav-item"><a href="/" className="nav-link px-2 text-light">Brazil</a></li>
                            <li className="nav-item"><a href="/" className="nav-link px-2 text-light">Canada</a></li>
                            <li className="nav-item"><a href="/" className="nav-link px-2 text-light">China</a></li>
                            <li className="nav-item"><a href="/" className="nav-link px-2 text-light">France</a></li>
                            <li className="nav-item"><a href="/" className="nav-link px-2 text-light">Germany</a></li>
                            <li className="nav-item"><a href="/" className="nav-link px-2 text-light">Italy</a></li>
                            <li className="nav-item"><a href="/" className="nav-link px-2 text-light">Japan</a></li>
                            <li className="nav-item"><a href="/" className="nav-link px-2 text-light">Mexico</a></li>
                            <li className="nav-item"><a href="/" className="nav-link px-2 text-light">Netherlands</a></li>
                            <li className="nav-item"><a href="/" className="nav-link px-2 text-light">Poland</a></li>
                            <li className="nav-item"><a href="/" className="nav-link px-2 text-light">Singapore</a></li>
                            <li className="nav-item"><a href="/" className="nav-link px-2 text-light">Spain</a></li>
                            <li className="nav-item"><a href="/" className="nav-link px-2 text-light">Turkey</a></li>
                            <li className="nav-item"><a href="/" className="nav-link px-2 text-light">United Arab Emirates</a></li>
                            <li className="nav-item"><a href="/" className="nav-link px-2 text-light">United Kingdom</a></li>
                            <li className="nav-item"><a href="/" className="nav-link px-2 text-light">United States</a></li>
                        </ul>
                    </footer>
                </div>

            </>
        )
    }
}

export default MidFooter;