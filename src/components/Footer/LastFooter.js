import React, { Component } from 'react'

class LastFooter extends Component {
    render() {
        return (
            <>
                <div className="container">
                    <footer style={{ fontSize: 12 }} className="row row-cols-1 row-cols-sm-2 row-cols-md-5 pt-5 mt-3 lh-1">

                        <div className="col-md-2.5 mb-3">
                            <ul className="nav flex-column w-50">
                                <li className="nav-item mb-2"><a href="/" className="nav-link p-0 text-muted">AbeBooks
                                    Books, art
                                    & collectibles</a></li>
                            </ul>
                        </div>

                        <div className="col-md-2.5 mb-3">
                            <ul className="nav flex-column w-50">
                                <li className="nav-item mb-2"><a href="/" className="nav-link p-0 text-muted"> 	Amazon Web Services
                                    Scalable Cloud
                                    Computing Services</a></li>
                            </ul>
                        </div>

                        <div className="col-md-2.5 mb-3">
                            <ul className="nav flex-column w-50">
                                <li className="nav-item mb-2"><a href="/" className="nav-link p-0 text-muted"> 	Audible
                                    Download
                                    Audio Books</a></li>
                            </ul>
                        </div>

                        <div className="col-md-2.5 mb-3">
                            <ul className="nav flex-column w-50">
                                <li className="nav-item mb-2"><a href="/" className="nav-link p-0 text-muted"> 	DPReview
                                    Digital
                                    Photography</a></li>
                            </ul>
                        </div>

                        <div className="col-md-2.5 mb-3">
                            <ul className="nav flex-column w-50">
                                <li className="nav-item mb-2"><a href="/" className="nav-link p-0 text-muted"> 	IMDb
                                    Movies, TV
                                    & Celebrities</a></li>
                            </ul>
                        </div>

                        <div className="col-md-2.5 mb-3">
                            <ul className="nav flex-column w-50">
                                <li className="nav-item mb-2"><a href="/" className="nav-link p-0 text-muted"> 	Shopbop
                                    Designer
                                    Fashion Brands</a></li>
                            </ul>
                        </div>

                        <div className="col-md-2.5 mb-3">
                            <ul className="nav flex-column w-50">
                                <li className="nav-item mb-2"><a href="/" className="nav-link p-0 text-muted">Amazon Business
                                    Everything For
                                    Your Business</a></li>
                            </ul>
                        </div>

                        <div className="col-md-2.5 mb-3">
                            <ul className="nav flex-column w-50">
                                <li className="nav-item mb-2"><a href="/" className="nav-link p-0 text-muted"> 	Prime Now
                                    2-Hour Delivery
                                    on Everyday Items</a></li>
                            </ul>
                        </div>

                        <div className="col-md-2.5 mb-3">
                            <ul className="nav flex-column w-50">
                                <li className="nav-item mb-2"><a href="/" className="nav-link p-0 text-muted">Amazon Prime Music
                                    90 million songs, ad-free
                                    Over 15 million podcast episodes </a></li>
                            </ul>
                        </div>

                    </footer>
                    <ul style={{ fontSize: 12 }} className="nav justify-content-center pb-3">
                        <li className="nav-item"><a href="/" className="nav-link px-2 text-light">Conditions of Use & Sale</a></li>
                        <li className="nav-item"><a href="/" className="nav-link px-2 text-light">Privacy Notice</a></li>
                        <li className="nav-item"><a href="/" className="nav-link px-2 text-light">Interest-Based Ads</a></li>
                        <li className="nav-item"><a href="/" className="nav-link px-2 text-light">&copy; 1996-2022, Amazon.com, Inc. or its affiliates</a></li>

                    </ul>
                </div>
            </>
        )
    }
}

export default LastFooter;