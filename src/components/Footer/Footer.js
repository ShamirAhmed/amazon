import React, { Component } from 'react'
import BackToTop from './BackToTop';
import LastFooter from './LastFooter';
import MidFooter from './MidFooter';

class Footer extends Component {
    render() {
        return (
            <>
                <BackToTop />
                <div className="container">
                    <footer className="row row-cols-1 row-cols-sm-2 row-cols-md-5 py-5 mt-5">

                        <div className="col-md-3 mb-3">
                            <h5 className='text-light'>Get to Know us</h5>
                            <ul className="nav flex-column">
                                <li className="nav-item mb-2"><a href="/" className="nav-link p-0 text-muted">About Us</a></li>
                                <li className="nav-item mb-2"><a href="/" className="nav-link p-0 text-muted">Careers</a></li>
                                <li className="nav-item mb-2"><a href="/" className="nav-link p-0 text-muted">Press Releases</a></li>
                                <li className="nav-item mb-2"><a href="/" className="nav-link p-0 text-muted">Amazon Cares</a></li>
                                <li className="nav-item mb-2"><a href="/" className="nav-link p-0 text-muted">Gift a Smile</a></li>
                                <li className="nav-item mb-2"><a href="/" className="nav-link p-0 text-muted">Amazon Science</a></li>
                            </ul>
                        </div>

                        <div className="col-md-3 mb-3">
                            <h5 className='text-light'>Connect with Us</h5>
                            <ul className="nav flex-column">
                                <li className="nav-item mb-2"><a href="/" className="nav-link p-0 text-muted">Facebook</a></li>
                                <li className="nav-item mb-2"><a href="/" className="nav-link p-0 text-muted">Twitter</a></li>
                                <li className="nav-item mb-2"><a href="/" className="nav-link p-0 text-muted">Instagram</a></li>
                            </ul>
                        </div>

                        <div className="col-md-3 mb-3">
                            <h5 className='text-light'>Make Money with Us</h5>
                            <ul className="nav flex-column">
                                <li className="nav-item mb-2"><a href="/" className="nav-link p-0 text-muted">Sell on Amazon</a></li>
                                <li className="nav-item mb-2"><a href="/" className="nav-link p-0 text-muted">Sell under Amazon Accelerator</a></li>
                                <li className="nav-item mb-2"><a href="/" className="nav-link p-0 text-muted">Amazon Global Selling</a></li>
                                <li className="nav-item mb-2"><a href="/" className="nav-link p-0 text-muted">Become an Affiliate</a></li>
                                <li className="nav-item mb-2"><a href="/" className="nav-link p-0 text-muted">Fulfilment by Affiliate</a></li>
                                <li className="nav-item mb-2"><a href="/" className="nav-link p-0 text-muted">Advertise Your Product</a></li>
                                <li className="nav-item mb-2"><a href="/" className="nav-link p-0 text-muted">Amazon Pay on Merchants</a></li>
                            </ul>
                        </div>

                        <div className="col-md-3 mb-3">
                            <h5 className='text-light'>Let Us Help You</h5>
                            <ul className="nav flex-column">
                                <li className="nav-item mb-2"><a href="/" className="nav-link p-0 text-muted">COVID-19 and Amazon</a></li>
                                <li className="nav-item mb-2"><a href="/" className="nav-link p-0 text-muted">Your Account</a></li>
                                <li className="nav-item mb-2"><a href="/" className="nav-link p-0 text-muted">Return Center</a></li>
                                <li className="nav-item mb-2"><a href="/" className="nav-link p-0 text-muted">100% Purchase Protection</a></li>
                                <li className="nav-item mb-2"><a href="/" className="nav-link p-0 text-muted">Amazon App Download</a></li>
                                <li className="nav-item mb-2"><a href="/" className="nav-link p-0 text-muted">Amazon Assistant Download</a></li>
                                <li className="nav-item mb-2"><a href="/" className="nav-link p-0 text-muted">Help</a></li>
                            </ul>
                        </div>
                    </footer>
                </div>
                <div className='border-top border-secondary'>
                    <MidFooter />
                </div>
                <div style={{backgroundColor: '#131a22'}}>
                    <LastFooter />
                </div>
            </>
        )
    }
}

export default Footer;