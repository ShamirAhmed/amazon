import React, { Component } from 'react'

class BackToTop extends Component {
    render() {
        return (
            <>
                <div style={{background:'#37475a'}} className='p-3 text-center text-light'>
                    Back To Top
                </div>
            </>
        )
    }
}

export default BackToTop;