import React, { Component } from 'react';
import './SideScrole.css';

class SideScrole extends Component {
    render() {
        return (
            <>
                <h4 className='mt-5'>Today's Deals</h4>
                <div className="wrapper px-5">
                    <div className='items d-flex flex-column'>
                        <img src='https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/410UOtOGvsL._AC_SY200_.jpg' alt='img' />
                        <div style={{fontSize: 12}} className='my-3'>
                            <span style={{backgroundColor: '#cc0c39'}} className="text-light p-2">Up to 29% off</span>
                            <span style={{color: '#cc0c39'}} className='fw-bold mx-2'>Deal of the Day</span>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default SideScrole;